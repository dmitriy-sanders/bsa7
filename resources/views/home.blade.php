@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Home</h4>
        </div>
        <div class="card-body">
            <h3>Welcome to the home - <u>{{ Auth::user()->name }}</u></h3>
            <p>You have authenticated! Now, you can visit <a href="{{ route('products') }}">products</a> page.</p>
        </div>
    </div>
@endsection

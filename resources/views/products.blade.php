@extends('layouts.app')

@section('content')
    <div class="header border-bottom mb-3">
        <h3 class="text-center">Products</h3>
        @can('create', \App\Entity\Product::class)
        <div class="text-center">
            <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#addModal">
                Add product
            </button>
        </div>
        @endcan
    </div>
    <div>
        @if(session('success'))
            <p class="alert alert-success">
                {{ session('success') }}
            </p>
        @elseif(session('fail'))
            <p class="alert alert-danger">
                {{ session('fail') }}
            </p>
        @endif
    </div>
    <div class="col-md-12 table-responsive">
        @if($products)
            <table class="table full-width">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Price, $</th>
                    <th>Seller Name</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td><a href="/products/{{ $product->id }}">{{ $product->name }}</a></td>
                        <td>{{ $product->price }} $</td>
                        <td>{{ $product->user->name }}</td>
                        <td>
                            @can('delete', $product)
                                <a href="{{ route('oneProduct', ['id' => $product->id]) }}">Go to edit!</a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h1>There is no products :(</h1>
        @endif
    </div>

    <div class="pagination justify-content-center">
        {{ $products->links() }}
    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Add Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('addProduct') }}" method="POST">
                <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="name">Product Name</label>
                            <input type="text" name="name" value="{{ old('name') }}"
                                   class="form-control @error('name') is-invalid @enderror" placeholder="Product name...">
                            @error('name')
                            <small class="text-danger">
                                <strong>{{ $message }}</strong>
                            </small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price">Product price</label>
                            <input type="text" name="price" value="{{ old('price') }}"
                                   class="form-control @error('price') is-invalid @enderror" placeholder="Product price...">
                            @error('price')
                                <small class="text-danger">
                                    <strong>{{ $message }}</strong>
                                </small>
                            @enderror
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-success" value="Add Product">
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

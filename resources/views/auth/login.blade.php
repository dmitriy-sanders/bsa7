@extends('layouts.app')

@section('content')
    <div class="align-content-center">
        <div class="login-form card col-md-4" style="margin: 0 auto">
            <div class="card-header" style="background: #fff">
                <h3 class="text-center">Login</h3>
            </div>
            <div class="card-body">
                <form action="" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Enter email">

                        @error('email')
                        <small>
                            <strong class="text-danger">{{ $message }}</strong>
                        </small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password">
                        @error('password')
                        <small>
                            <strong class="text-danger">{{ $message }}</strong>
                        </small>
                        @enderror
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="remember" name="remember">
                        <label class="form-check-label" for="remember">Remember</label>
                    </div>
                    <div class="input-group mt-3 justify-content-center col-md-12">
                        <input type="submit" class="btn btn-warning" value="Login">
                    </div>
                    <br>
                    <div class="text-center">
                        <h4 class="mb-0">Login via socials</h4>
                        <small>Will be registered if wasn't yet</small>
                        <a href="{{ route('githubLogin') }}" class="btn btn-secondary">Login via GitHub</a><br><br>
                        <a href="{{ route('facebookLogin') }}" class="btn btn-primary">Login via Facebook</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

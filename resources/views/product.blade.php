@extends('layouts.app')

@section('content')
    <div class="header border-bottom mb-3">
        <h3 class="text-center">
            <a href="{{ route('products') }}">Return to Products</a>
        </h3>
    </div>
    @if(session('success'))
        <p class="alert alert-success">
            {{ session('success') }}
        </p>
    @elseif(session('fail'))
        <p class="alert alert-danger">
            {{ session('fail') }}
        </p>
    @endif
    <div class="row table-responsive">
        <table class="table full-width">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th>Seller Name</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $product->id }}</td>
                <td><a href="/products/{{ $product->id }}">{{ $product->name }}</a></td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->user->name }}</td>
                <td>

                    @can('delete', $product)
                        <button type="button" class="btn btn-danger mb-3" data-toggle="modal" data-target="#deleteModal">
                            Delete
                        </button>
                    @endcan

                    @can('update', $product)
                        <button type="button" class="btn btn-warning mb-3" data-toggle="modal" data-target="#editModal">
                            Edit
                        </button>
                    @endcan
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 class="text-danger">Are you sure you want delete <u>{{ $product->name }}</u>?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <form action="/products/{{ $product->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Edit Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/products/{{ $product->id }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Product Name</label>
                            <input type="text" name="name" value="{{ $product->name }}"
                                   class="form-control @error('name') is-invalid @enderror" placeholder="Product name...">
                            @error('name')
                            <small class="text-danger">
                                <strong>{{ $message }}</strong>
                            </small>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="price">Product price</label>
                            <input type="text" name="price" value="{{ $product->price }}"
                                   class="form-control @error('price') is-invalid @enderror" placeholder="Product price...">
                            @error('price')
                            <small class="text-danger">
                                <strong>{{ $message }}</strong>
                            </small>
                            @enderror
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <input type="submit" class="btn btn-warning" value="Update Product">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

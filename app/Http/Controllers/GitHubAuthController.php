<?php

namespace App\Http\Controllers;

use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class GitHubAuthController extends Controller
{
    protected $redirectTo = '/products';

    public function redirectToProvider(Request $request)
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleProviderCallback()
    {
        $userFound = Socialite::driver('github')->user();

        $email = $userFound->email;


        $user = User::where(['email' => $email])->first();

        if (is_null($user)) {
            User::create([
                'name' => $userFound->getNickname(),
                'email' => $email,
                'password' => Str::random(20)
            ]);
        }

        $newUser = $user ?? User::where(['email' => $email])->first();

        Auth::login($newUser);

        return redirect($this->redirectTo);
    }
}

<?php

namespace App\Http\Controllers;

use App\Entity\Product;

use Illuminate\Http\Request;

class MarketplaceController extends Controller
{
    public function index()
    {
        return view('marketplace');
    }

    public function productsCollection(Request $request)
    {
        if ($request->user()->can('viewAny', Product::class)) {
            return view('products', [
                'products' => Product::orderBy('id', 'desc')->paginate(10)
            ]);
        } else {
            return redirect(route('login'));
        }
    }

    public function getProductById(Request $request, string $id)
    {
        $product = Product::find($id);

        if ($request->user()->can('view', $product)) {
            return view('product', [
                'product' => $product
            ]);
        } else {
            return redirect(route('products'));
        }
    }

    public function createProduct(Request $request)
    {
        if ($request->user()->can('create', Product::class)) {
            $request->validate([
                'name' => 'required',
                'price' => 'required|numeric'
            ]);

            $product = Product::create([
                'name' => $request->name,
                'price' => $request->price,
                'user_id' => $request->user()->id
            ]);

            if($product) {
                return redirect()
                    ->route('products')
                    ->with(['success' => "Product was added!"]);
            } else {
                return redirect()
                    ->route('products')
                    ->with(['fail' => "Product wasn't added!"]);
            }
        } else {
            return redirect()
                ->route('products');
        }
    }

    public function updateProduct(Request $request, string $id)
    {
        $product = Product::find($id);

        if (!is_null($product)) {
            if ($request->user()->can('update', $product)) {
                $request->validate([
                    'name' => 'required',
                    'price' => 'required|numeric'
                ]);

                $product->name = $request->name;
                $product->price = $request->price;
                $product->save();

                return redirect()
                    ->route('oneProduct', ['id' => $product->id])
                    ->with(['success' => "Product was updated"]);
            } else {
                return redirect()
                    ->route('oneProduct', ['id' => $product->id])
                    ->with(['fail' => "Something went wrong while editing product"]);
            }
        } else {
            return redirect()
                ->route('products')
                ->with(['fail' => "Product wasn't found!"]);
        }

    }

    public function deleteProduct(Request $request, string $id)
    {
        $product = Product::find($id);

        if($request->user()->can('delete', $product)) {
            $product->delete();
        } else {
            return redirect(route('products'))
                ->with(['fail' => "You can't delete this post"]);
        }

        return redirect(route('products'))
            ->with(['success' => 'Product was deleted!']);
    }
}

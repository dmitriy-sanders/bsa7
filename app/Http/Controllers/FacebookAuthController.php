<?php

namespace App\Http\Controllers;

use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class FacebookAuthController extends Controller
{
    protected $redirectTo = '/products';

    public function redirectToProvider(Request $request)
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $userFound = Socialite::driver('facebook')->user();

        $email = $userFound->getEmail();

        if (is_null($email)) {
            return redirect(route('login'))->withErrors(["email" => "Oops! Unfortunately we can't find email to register you :("]);
        }

        $user = User::where(['email' => $email])->first();

        if (is_null($user)) {
            User::create([
                'name' => $userFound->getName(),
                'email' => $email,
                'password' => Str::random(20)
            ]);
        }

        $newUser = $user ?? User::where(['email' => $email])->first();

        Auth::login($newUser);

        return redirect($this->redirectTo);
    }
}
